---
layout: post
title:  "Design for multi-day events"
description: " Creating a new feature for University of Toronto: Faculty of Arts and Science"
date:   2018-01-3 16:16:01 -0600
categories: ["Design"]
---

<!-- Overview -->
When I was brought onboard at University of Toronto: Faculty of Arts and Science, the Departmental Website Redesign project was underway in standardizing website templates for their faculty using Drupal. Buried in the requirements was the option for departments to promote and host multi-day events. Designing this module took place at where most of work takes place, the white board!

![White Board UofT]({{ "/images/whiteboard_uoft.jpg" | absolute_url }})

I started by defining what makes up an event that could be considered variables: date, time, location. This lead me to mark down each different combination that's possible. It ended up being a grand total of 8 possible options. The front customer facing page came first as I knew this would be the biggest challenge.

![UofT Accordion ]({{ "/images/uoft_accordion.JPG" | absolute_url }})

I decided that the best course of action would be have collapsible accordions for each separate events. This would be called an event program. Here would hold: type of event, date and time, location, description, map, add to calendar, and purchase tickets link. As a event date passes, the event will be collapsed when a new user comes to the page. Once the accordion came together the rest fell in place. On the individual event page users would see the "Branded Title" of the event. Followed by the date(s).

In the end this design offered the flexibility the stakeholders were looking for as well a nice clean design that is easy to understand.

![Final Design]({{ "/images/uoft_final.JPG" | absolute_url }})



